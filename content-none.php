<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Amadeus
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Niczego nie znaleziono', 'amadeus' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p>
			<?php
				/* translators: Add new post link */
				printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'amadeus' ), esc_url( admin_url( 'post-new.php' ) ) );
			?>
				</p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Spróbuj innego zapytania!', 'amadeus' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'Spróbuj innego zapytania!', 'amadeus' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
