<?php
/**
 * The template for displaying all single posts.
 *
 * @package Amadeus
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) :
			the_post();
			?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php
			
				$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
				$next     = get_adjacent_post( false, '', false );

				if ( ! $next && ! $previous ) {
					return;
				}
				?>
				<nav class="navigation post-navigation" role="navigation">
					<h2 class="screen-reader-text"><?php _e( 'Post navigation', 'amadeus' ); ?></h2>
					<div class="nav-links">
						<?php
						previous_post_link( '<div class="nav-previous">%link</div>', 'Poprzedni post' );
						next_post_link( '<div class="nav-next">%link</div>', 'Następny post' );
						?>
					</div><!-- .nav-links -->
				</nav><!-- .navigation -->
				<?php
			
			?>

		<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
if ( ! get_theme_mod( 'hide_sidebar_single' ) ) {
	get_sidebar();
}
?>
<?php get_footer(); ?>
