<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Amadeus
 */
?>

	</div><!-- #content -->

	<?php if ( is_active_sidebar( 'sidebar-4' ) || is_active_sidebar( 'sidebar-5' ) || is_active_sidebar( 'sidebar-6' ) ) : ?>
		<?php get_sidebar( 'footer' ); ?>
	<?php endif; ?>

	<footer id="footer">
		<p class="sky-hide-desktop-footer">
			<em>Skylap</em> Serwis komputerowy <br> ul Świetlista 4a, Olsztyn<br> tel.: 503 624 820
			<span>Skylap &copy; wszelkie prawa zastrzeżone</span>
		</p>

		<p class="sky-hide-mobile-footer">
			<em>Skylap</em> Serwis komputerowy Olsztyn, ul Świetlista 4a, tel.: 503 624 820,
			<span>Skylap &copy; wszelkie prawa zastrzeżone</span>
		</p>

	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
