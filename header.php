<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Amadeus
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Blog">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="keywords" content="naprawa laptopów olsztyn, naprawa tabletów, naprawa komputerów, serwis komputerowy, obsługa informatyczna, pogotowie komputerowe, odzyskiwanie danych, sprzedaż laptopów, skup laptopów"/>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<title>Blog - Skylap Olsztyn</title>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'amadeus' ); ?></a>

	<header id="header">
		<!-- logo -->
		<a href="http://skylap.pl" id="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Skyap - Serwis komputerowy"></a>
		<!-- główne menu -->
		<nav id="top-menu">
			<h2 class='d-none'>Nawigacja desktop</h2>
			<?php wp_nav_menu([
				'menu' => 'header-navigation',
			]) ?>
		</nav>
		<!-- koniec głównego menu -->
	</header>

<div id="wrapper" class="site-content container">
